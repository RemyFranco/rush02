/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   h.h                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/30 11:00:42 by pmarquis          #+#    #+#             */
/*   Updated: 2022/07/30 11:19:53 by pmarquis         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

typedef struct
{
	char	*num;
	char	*word;
	t_entry	*next;
}	t_entry;


void	convert(char *nbr);
void	convert2(char *dict, char *nbr);

int		_isspace(char c):
void	_print(char *msg);
int		_strcmp(char *s1, char *s2);

